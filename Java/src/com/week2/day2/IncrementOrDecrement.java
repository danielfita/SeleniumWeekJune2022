package com.week2.day2;

public class IncrementOrDecrement {

	public static void main(String[] args) {


		//postIncrement
		//preIncrement
		//postdecrement
		//preDecrement
		
		//a++= (a=a+1);
		
		//postIncrement
		int a = 1;
		a++;
		System.out.println(a++);//2 2 1 2
		int b;
		b=a++;
		
		System.out.println(a);//2 2 2 2
		System.out.println(b);//2 1 2 1
		
		//preIncrement
		int c = 1;
		int d;
		d=++c;
		
		System.out.println(c);//2 1 2
		System.out.println(d);//2 2 2
		System.out.println("-------------------");
		
		//postDecrement
		
		int e = 1;
		e--;
		System.out.println(e--);//0 0
		int f;
		f=e--;
		
		System.out.println(e);//0 0
		System.out.println(f);//0 1
		
		//preDecrement
		
		int g = 10;
		--g;
		System.out.println(--g); // 9 9
		int h;
		h=--g;
		System.out.println(--h); // 9 8
		System.out.println(--g); // 8 8
		
		
	}

}
