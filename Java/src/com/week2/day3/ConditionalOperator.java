package com.week2.day3;

public class ConditionalOperator {

	public static void main(String[] args) {

		//if
		//if else
		//if else if
		// nested if
		
		//if else
		int a=9;
		
		if(a>10)
		{
			System.out.println("a is greater than 10");
		}
		else
		{
			System.out.println("a is not greater than 10");
		}
		
		System.out.println("------------------------");
		
		//if else if
		int b,c,d;
		b=300;
		c=450;
		d=200;
		
		if(b>c && b>d)
		{
			System.out.println("b is greatest");
		}
		else if (c>d)
		{
			System.out.println("c is greatest");
		}
		else
		{
			System.out.println("d is greatest");
		}
		
		System.out.println("------------------------");

		//nested if
		
		int mark = 90;
		
		if(mark>90)
		{
			if(mark>95)
			{
				System.out.println("grade A++");
			}
			else
			{
				System.out.println("grade A");
			}
				
		}
		else
		{
			System.out.println("grade B");
		}
		System.out.println("------------------------");

		if(false) //Dead code
		{
			System.out.println("true statement");
		}
	}

}
