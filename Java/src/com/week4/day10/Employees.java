package com.week4.day10;

public class Employees {
	
	//Constructor
	//1. Constructor method name should be equal to class name
	//2. Constructor method would be called when object is created
	//3. It will not return anything as no return type. 
	//4. Types - 1. parameterized and 2. non parameterized
	
	public Employees() {
		System.out.println("I'm from employee default constructor");
	}
	public Employees(String a) {
		
		System.out.println("fita"+a);
	}
	
	
	public static void main(String[] args) {

		Employees emp1 = new Employees("class");
		emp1.testMethod();
		
		
	}
	public void testMethod()
	{
		System.out.println("im from testmethod");
	}

}
