package com.week4.day10;

public class Methodoverloading {
	
	//Method overloading
	//1. Same method name 
	//2. Different number of argument/ different type of argument
	
	public void methodA() {
	System.out.println("iam from method A");
	
	}
	public void methodA(int a) {
		System.out.println("adding given number with 10 "+(a+10));
	
	}
	public void methodA(int a,int b) {
		System.out.println("sum of two given numbers "+(a+b));
	}
	public void methodA (String a) {
		System.out.println(a);
	}
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Methodoverloading m1 = new Methodoverloading();
		m1.methodA();
		m1.methodA(34);
		m1.methodA(10, 20);
		m1.methodA("Sathya");
		
	}

}
