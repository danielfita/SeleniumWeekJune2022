package com.week5.day13;

public interface IndianMedical {

	public void neuro();

	public void ortho();

	default void medicalInsurance() {
		System.out.println("im-medicalInsurance");
	}

	public static void billing() {
		System.out.println("im-billing");
	}
	
}
