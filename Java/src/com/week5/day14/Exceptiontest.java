package com.week5.day14;

public class Exceptiontest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int c = 9;

		try {
			System.out.println(c / 2);
			String[] str = new String[3];
			str[2] = "name";
			Test t = new Test();
//			t = null;
			t.age = 23;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("catchblockexception");
			e.printStackTrace();
			
		}
		finally
		{
			System.out.println("I'm from finally block. I always execute");
		}
	}

}
