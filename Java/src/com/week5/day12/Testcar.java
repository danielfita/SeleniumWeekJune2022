package com.week5.day12;

public class Testcar {

public static void main(String[] args) {
	Swift s = new Swift();
	s.start();
	s.horn();
	s.transmission();
	s.stop();
	System.out.println("------------");
	Audi a = new Audi();
	a.start();
	a.horn();
	a.transmission();
	a.stop();
	a.parkingAssistance();
	
	System.out.println("---------");
	
	Auto at = new Auto();
	at.start();
	at.stop();
}

}
