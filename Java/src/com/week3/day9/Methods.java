package com.week3.day9;

public class Methods {

	public static void main(String[] args) {

		System.out.println("I'm from main");

		Methods meth1 = new Methods(); // object Instanziation
		meth1.methodA(); // method calling
		meth1.methodB();
		
		meth1.addTwoNumbers(2, 5);
//		int q= meth1.addAndReturn();
		System.out.println(meth1.addAndReturn());
		testMethod();
		
		System.out.println(meth1.multiply(2,7));
		System.out.println(meth1.name("sathya","moorthy"));
		
		
	}

	// no arguments no return type
	public void methodA() {
		System.out.println("I'm from methodA");

	}

	// no arguments no return type
	public void methodB() {
		System.out.println("I'm from methodB");
	}

	// with arguments no return type
	public void addTwoNumbers(int a, int b) {
		System.out.println(a + b);
	}
	
	//no arguments with return type
	public int addAndReturn() {
		int a,b,c;
		a=5;
		b=9;
		c=a+b;
		
		return c;
	}
	
	// no arguments no return type
	public static void testMethod() {
		System.out.println("I'm from testMethod");

	}
	
	//with arguments with return type
	public int multiply(int a,int b) {
		int c;
		c=a*b;
		return c;
		
	}
	
	public String name(String str1,String str2) {
		return str1+str2;
		

	}
	
	
	
	
	
	
	
	
	
	
	
	
	

}
