package com.week3.day8;

public class Employee {
	String name;
	int empid;
	double salary;
	char gender;
	boolean isActive;
	
	public static void main(String[] args) {
		Employee e1=new Employee();
		e1.name="daniel";
		e1.empid=001;
		e1.salary=15000.00;
		e1.gender='m';
		e1.isActive=true;
		System.out.println(e1.name+" "+e1.empid+" "+e1.salary+" "+e1.gender+" "+e1.isActive);
		
		Employee e2 = new Employee();
		e2.name="sathya";
		e2.empid=2134;
		e2.salary=23000.00;
		e2.gender='M';
		e2.isActive=true;
		System.out.println(e2.name+" "+e2.empid+" "+e2.salary+" "+e2.gender+" "+e2.isActive);
		
		e2 = null;
		
//		e2.name="Ram"; //java.lang.NullPointerException
		e1.test();
	}
	
	public void test() {
		
		this.name = "Ram";
		System.out.println(name);
		
	}

}
