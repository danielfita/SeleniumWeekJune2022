package com.week3.day7;

import java.util.ArrayList;

public class DynamicArray {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) {
		
		ArrayList al = new ArrayList(5);
		al.add("Ram");
		al.add(123);
		al.add('M');
		al.add(true);
		al.add("b+");
		al.add(15000.00);
		
		System.out.println(al);
		
		for(int i=0;i<al.size();i++)
		{
			System.out.println(al.get(i));
		}
		
		System.out.println("**************************************************");

		
		System.out.println(al);
		al.remove(4);
		System.out.println(al);
		al.remove("Ram");
		System.out.println(al);
		
		System.out.println("**************************************************");

		//System.out.println(al.get(4)); java.lang.IndexOutOfBoundsException:
		
		al.add(0,"Ram");
		System.out.println(al);
		System.out.println(al.size());


	}

}
