package com.week3.day6;


public class StaticArrays {

	public static void main(String[] args) {
		
		String[] str = new String[3];

		str[0] = "Ram";
		str[1] = "sita";
		str[2] = "ravi";
		
		System.out.println(str);
		
		System.out.println("----------");

		
		for (int i = 0; i < str.length; i++) {
			System.out.println(str[i]);

		}
		// str[3] ="abbas";//java.lang.ArrayIndexOutOfBoundsException:
		System.out.println("----------");
		String[] str1 = { "daniel", "satya", "abbas", "fita" };
		for (int i = 0; i < str1.length; i++) {
			System.out.println(str1[i]);

		}

	}
}
