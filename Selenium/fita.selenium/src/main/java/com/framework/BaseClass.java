package com.framework;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

public class BaseClass {
	
	public static WebDriver driver;
//	public static ThreadLocal<WebDriver> driver = new ThreadLocal<>();

	@BeforeSuite
	public void startReport() {

		System.out.println("BS - StartReport");

	}

	@AfterSuite
	public void endReport() {

		System.out.println("AS - endReport");

	}

	@BeforeClass
	public void openBrowser() {

		System.out.println("BC - openBrowser");
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.manage().window().maximize();
		driver.get("https://www.amazon.in/");

	}

	@AfterClass
	public void quitBrowser() {

		System.out.println("AC - quitBrowser");
		driver.quit();

	}

	@BeforeMethod
	public void beforeMethod() {

		System.out.println("BM - beforeMethod");

	}

	@AfterMethod
	public void afterMethod() {

		System.out.println("AM - afterMethod");

	}

}
