package com.framework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BrowserUtils {

	WebDriver driver;

	public void launchBrowser(String browser) {

		System.out.println("Selected browser is: " + browser);

		if (browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
		} else if (browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
			driver = new FirefoxDriver();
		} else if (browser.equalsIgnoreCase("edge")) {
			System.setProperty("webdriver.edge.driver", "./drivers/edgedriver.exe");
			driver = new EdgeDriver();
		} else if (browser.equalsIgnoreCase("ie")) {
			System.setProperty("webdriver.ie.driver", "./drivers/IEDriverServer.exe");
			driver = new EdgeDriver();
		}

	}

	public void passUrl(String url) {
		if (url == null) {
			System.out.println("URL is null. Please pass right URL");
		}
		if (url.contains("https") || (url.contains("http"))) {
			driver.get(url);
		} else {
			System.out.println("URL doesn't contain http(s). Please pass right URL");
		}
	}

	public String getCurrentTitle() {
		return driver.getTitle();
	}

	public void quitDriver() {
		driver.quit();
	}

	public WebElement getByElement(String locator, String element) {

		switch (locator.toLowerCase()) {
		case "id":
			return driver.findElement(By.id(element));
		case "name":
			return driver.findElement(By.name(element));
		case "classname":
			return driver.findElement(By.className(element));
		case "xpath":
			return driver.findElement(By.xpath(element));
		case "cssselector":
			return driver.findElement(By.cssSelector(element));
		case "linktext":
			return driver.findElement(By.linkText(element));
		case "partiallinktext":
			return driver.findElement(By.partialLinkText(element));
		case "tagname":
			return driver.findElement(By.tagName(element));
		default:
			System.out.println("Please pass a right locator");
			return null;
		}

	}
	
	public void enterKeys(String locator, String element, String value) {
		getByElement(locator, element).sendKeys(value);
	}
	
	public void clickElement(String locator, String element) {
		getByElement(locator, element).click();
	}

}
