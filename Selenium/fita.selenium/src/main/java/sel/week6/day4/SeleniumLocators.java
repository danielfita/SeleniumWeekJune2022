package sel.week6.day4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumLocators {
	
	static WebDriver driver;


	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		
		driver.get("https://www.opencart.com/index.php?route=account/login");
		
		//1. id
//		driver.findElement(By.id("input-email")).sendKeys("test@email.com");
		
		//2. name
//		driver.findElement(By.name("email")).sendKeys("test@email.com");
		
		//3. classname
//		driver.findElement(By.className("form-control")).sendKeys("test");
		
		//4.Xpath
	//	driver.findElement(By.xpath("//*[@id=\"input-email\"]")).sendKeys("test");
		
		//5.css selector
	//	driver.findElement(By.cssSelector("#input-email")).sendKeys("test");
		//6.link text
//		Thread.sleep(2000);
//		driver.findElement(By.linkText("REGISTER")).click();
		
		//7. PARTIAL LINK TEXT
		Thread.sleep(2000);
		driver.findElement(By.partialLinkText("REGIS")).click();
		Thread.sleep(2000);
		
		//8. tag name
		driver. findElement(By.tagName("a")).click();

		
		
	}

}
