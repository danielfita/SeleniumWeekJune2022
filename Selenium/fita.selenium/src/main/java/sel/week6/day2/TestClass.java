package sel.week6.day2;

import com.framework.BrowserUtils;

public class TestClass {

	public static void main(String[] args) {

		BrowserUtils bu = new BrowserUtils();
		bu.launchBrowser("chrome");
		bu.passUrl("https://www.amazon.in/");
		String title = bu.getCurrentTitle();

		if (title.equals("Amazon.com. Spend less. Smile more.")) {
			System.out.println("correct title");
		} else {
			System.out.println("incorrect title");
		}

		bu.quitDriver();

	}

}
