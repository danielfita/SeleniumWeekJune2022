package sel.week6.day3;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

//import io.github.bonigarcia.wdm.WebDriverManager;

public class LoginTest {
	
	 //mohamedabbasofficial25@gmail.com
	//Abu@2510
	
	static WebDriver driver;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
//		WebDriverManager.chromedriver().setup();
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		
		driver.get("https://www.opencart.com/index.php?route=account/login");
		
		//1 way
//		driver.findElement(By.id("input-email")).sendKeys("mohamedabbasofficial25@gmail.com");
//		driver.findElement(By.id("input-password")).sendKeys("Abu@2510");
		
		//2 way
//		WebElement uname= driver.findElement(By.id("input-email"));
//		WebElement pwd  = driver.findElement(By.id("input-password"));
//		uname.sendKeys("mohamedabbasofficial25@gmail.com");
//		pwd.sendKeys("Abu@2510");
		
		//3 way
//		By uNameId = By.id("input-email");
//		By pwdId = By.id("input-password");
//		driver.findElement(uNameId).sendKeys("mohamedabbasofficial25@gmail.com");
//		driver.findElement(pwdId).sendKeys("Abu@2510");
		
		//4th way
//		LoginTest.getEmailElement().sendKeys("mohamedabbasofficial25@gmail.com");
//		LoginTest.getPwdElement().sendKeys("Abu@2510");
		
		//5th way
		getEmailElement("mohamedabbasofficial25@gmail.com");
		getPwdElement("Abu@2510");

	}
	
	public static WebElement getEmailElement() {
		return driver.findElement(By.id("input-email")); 
	}
	
	public static WebElement getPwdElement() {
		return driver.findElement(By.id("input-password"));
	}
	
	public static void getEmailElement(String value) {
		driver.findElement(By.id("input-email")).sendKeys(value);
	}
	
	public static void getPwdElement(String value) {
		driver.findElement(By.id("input-password")).sendKeys(value);
	}

}
