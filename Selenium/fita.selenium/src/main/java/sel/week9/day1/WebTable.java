package sel.week9.day1;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTable {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.techlistic.com/p/demo-selenium-practice.html");
//		String table = driver.findElement(By.id("customers")).getText();
//		System.out.println(table);
		
		String cname = "Burj Khalifa";
		
		String contact = driver.findElement(By.xpath("//th[text()='"+cname+"']/following-sibling::td[1]")).getText();
		String country = driver.findElement(By.xpath("//th[text()='"+cname+"']/following-sibling::td[2]")).getText();
		
		System.out.println("contact of "+cname+" is "+contact );
		System.out.println("country of "+cname+" is "+country );
		
		driver.quit();


		
					
		}
		
		
	}


