package sel.week9.day1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class ActionClass {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://jqueryui.com/droppable/");
		
		driver.switchTo().frame(driver.findElement(By.className("demo-frame")));

		
		WebElement eleA = driver.findElement(By.id("draggable"));
		WebElement eleB = driver.findElement(By.id("droppable"));
		
		Thread.sleep(3000);
		
//		driver.switchTo().frame(0);

		
		Actions dnd = new Actions(driver);
		dnd.dragAndDrop(eleA, eleB).perform();
		
		


	}

}
