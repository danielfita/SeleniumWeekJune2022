package sel.week9.day3;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SelectorsXpath {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.get("https://selectorshub.com/xpath-practice-page/");

		while (true) {

			if (driver.findElements(By.xpath("//*[text()='Russia']")).size() > 0) {
				driver.findElement(By.xpath("//*[text()='Russia']/preceding-sibling::td/input[@type='checkbox']")).click();
				break;
			} else {
				WebElement next = driver.findElement(By.id("tablepress-1_next"));
				if (next.getAttribute("class").contains("disabled")) {
					System.out.println("No data found");
					break;
				} else {
					next.click();
				}

			}
		}

	}
}
