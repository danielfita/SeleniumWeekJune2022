package sel.week8.day3;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class MultipleWindowHandle {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://opensource-demo.orangehrmlive.com/index.php/auth/login");

		driver.findElement(By.xpath("//img[@alt='LinkedIn OrangeHRM group']")).click();
		driver.findElement(By.xpath("//img[@alt='OrangeHRM on Facebook']")).click();
		driver.findElement(By.xpath("//img[@alt='OrangeHRM on twitter']")).click();
		driver.findElement(By.xpath("//img[@alt='OrangeHRM on youtube']")).click();

		String parentWinId = driver.getWindowHandle();
		Set<String> allWindow = driver.getWindowHandles();

		for (String win : allWindow) {
			driver.switchTo().window(win);
			String title = driver.getTitle();
			System.out.println(title);
//			Thread.sleep(5000);
			if (parentWinId.equals(win) ) {
			}
			else if(title.equals("OrangeHRM Inc - YouTube")){
				
			}
			else
			{
				driver.close();
			}
			
		}

//		driver.quit();

	}

}
