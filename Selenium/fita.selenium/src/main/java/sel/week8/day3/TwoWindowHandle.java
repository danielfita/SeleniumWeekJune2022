package sel.week8.day3;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class TwoWindowHandle {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://opensource-demo.orangehrmlive.com/index.php/auth/login");
		
		driver.findElement(By.xpath("//img[@alt='LinkedIn OrangeHRM group']")).click();
		
		Set<String> allWindow = driver.getWindowHandles();
		
		List<String> allWin = new ArrayList<String>(allWindow);
		
		String parentWinId = allWin.get(0);
		String childWinId = allWin.get(1);
		
		System.out.println(parentWinId);
		System.out.println(childWinId);

		
		driver.switchTo().window(childWinId);
		String childTitle = driver.getTitle();
		System.out.println(childTitle);
		
		Thread.sleep(5000);
		driver.close();
		
		driver.switchTo().window(parentWinId);
		String parentTile = driver.getTitle();
		System.out.println(parentTile);
		
		driver.quit();

		
//		for(String win:allWindow)
//		{
//			System.out.println(win);
//		}
		
		
//	driver.quit();

	}

}
