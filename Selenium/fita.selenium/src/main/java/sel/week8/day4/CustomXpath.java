package sel.week8.day4;

public class CustomXpath {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//xpathsyntax
		
		//xpath with attribute
		//tagname[@attribute='value']
		//input[@id='input-email']
		
		
		//xpath with two attribute
		//input[@placeholder='Password' and @class='form-control']
		
		// text xpath
		//button[text()='Login']
		
		//xpath with text and attribute
		//button[text()='Login' and @type='submit']
		
		// text xpath with index
//		(//button[text()='Login'])[1]
		
		//parent to child
//		(//table[@id='customers']//tr)[4]/td
		//table[@class='navFooterMoreOnAmazon']//td/a
		
		//child to parent
		//*[text()='AbeBooks']/parent::td
		////*[text()='AbeBooks']/..
		
		//following sibling
		//*[text()='Microsoft']/following-sibling::td
		
		//preceding sibling
		//*[text()='Roland Mendel']/preceding-sibling::td
		

	}

}
