package sel.week8.day2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Frames {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://the-internet.herokuapp.com/nested_frames");
		
		driver.switchTo().frame("frame-top");
		driver.switchTo().frame("frame-middle");
		
		String middleText = driver.findElement(By.id("content")).getText();
		System.out.println(middleText);
		
		driver.switchTo().parentFrame();
		driver.switchTo().frame("frame-right");

		String rightText = driver.findElement(By.xpath("//body[contains(text(),'RIGHT')]")).getText();
		System.out.println(rightText);
		
		driver.switchTo().defaultContent();

		driver.switchTo().frame("frame-bottom");
		
		String bottomText = driver.findElement(By.xpath("//body[contains(text(),'BOTTOM')]")).getText();
		System.out.println(bottomText);
		
		driver.quit();

		
	}

}
