package sel.week7.day3;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

public class Dropdown {
	
	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		WebDriver driver = new ChromeDriver();
			
			driver.get("https://www.amazon.in/");
			
			WebElement ddElement = driver.findElement(By.id("searchDropdownBox"));
			
			Select s = new Select(ddElement);
			
//			s.selectByIndex(2);
//			s.selectByVisibleText("Appliances");
//			s.selectByValue("search-alias=mobile-apps");
			List<WebElement> options = s.getOptions();
			
			for(WebElement eachOption:options)
			{
				
				System.out.println(eachOption.getText());
			}
	
		
	}
	
	

}
