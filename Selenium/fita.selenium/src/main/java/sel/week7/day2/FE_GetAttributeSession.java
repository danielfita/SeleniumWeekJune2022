package sel.week7.day2;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FE_GetAttributeSession {
	
	static WebDriver driver;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();

		driver.get("https://www.amazon.in/");

		List<WebElement> links = driver.findElements(By.tagName("a"));
		List<String> list = new ArrayList<String>();
		
		for(WebElement link:links)
		{
			String hrefAttribute = link.getAttribute("href");
//			System.out.println(hrefAttribute);
			list.add(hrefAttribute);
		}
		
		int si = list.size();
		System.out.println(si);

		
		

	}

}
