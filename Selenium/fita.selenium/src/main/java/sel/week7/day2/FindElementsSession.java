package sel.week7.day2;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FindElementsSession {

	static WebDriver driver;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();

		driver.get("https://www.amazon.in/");

		List<WebElement> links = driver.findElements(By.tagName("a"));
		List<String> li = new ArrayList<String>();

		System.out.println(links.size());

		for (int i = 0; i < links.size(); i++) {
			String text = links.get(i).getText();
//			System.out.println(text);
			if (!text.isEmpty()) {
				li.add(text);
			}
		}
		System.out.println(li.size());

	}

}
