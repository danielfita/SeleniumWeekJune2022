package sel.week7.day1;

import com.framework.BrowserUtils;

public class SeleniumLocatorWithUtil {
	
	public static void main(String[] args) {
		BrowserUtils bu = new BrowserUtils();
		
		bu.launchBrowser("chrome");
		bu.passUrl("https://www.opencart.com/index.php?route=account/login");
		bu.enterKeys("id", "input-email", "test@email.com");
		bu.enterKeys("name", "password", "TestPassword");
		
		bu.clickElement("LINKTEXT", "REGISTER");
//		bu.quitDriver();
		
			}

}
