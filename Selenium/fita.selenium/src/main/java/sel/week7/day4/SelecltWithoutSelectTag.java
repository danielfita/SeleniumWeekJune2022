package sel.week7.day4;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SelecltWithoutSelectTag {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.google.com/");
		
		driver.findElement(By.name("q")).sendKeys("testing");
		
		Thread.sleep(3000);
		List<WebElement> search = driver.findElements(By.xpath("//ul//span"));
		for(WebElement link :search) {
			
			if(link.getText().contains("interview questions"))
			{
				link.click();
				break;
			}
//			System.out.println(link.getText());
		}
		
		
	}

}
