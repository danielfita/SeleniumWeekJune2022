package sel.week10.day2;

import org.testng.annotations.Test;

public class Priority {

	@Test(priority=1)
	public void a_method() {
		System.out.println("a_method");
	}

	@Test(priority=2)
	public void c_method() {
		System.out.println("c_method");
	}

	@Test(priority=3)
	public void d_method() {
		System.out.println("d_method");
	}

	@Test(priority=4)
	public void b_method() {
		System.out.println("b_method");
	}

	@Test(priority=5)
	public void e_method() {
		System.out.println("e_method");
	}

	@Test(priority=6)
	public void f_method() {
		System.out.println("f_method");
	}

}
