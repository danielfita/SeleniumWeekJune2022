package sel.week10.day2;

import org.testng.annotations.Test;

public class DependsOnMethod {
	
	@Test()
	public void loginTest() throws Exception {
		System.out.println("loginTest");
		throw new Exception();
	}

	@Test(dependsOnMethods="loginTest")
	public void homePageTest() {
		System.out.println("homePageTest");
	}

	@Test()
	public void searchPageTest() {
		System.out.println("searchPageTest");
	}

	

}
