package sel.week10.day2;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.framework.BaseClass;

public class AmazonURLTest extends BaseClass{
	
	@Test
	public void urlTest() {
		
		String url = BaseClass.driver.getCurrentUrl();		
		Assert.assertTrue(url.contains("amazon"));
		
	}

}
