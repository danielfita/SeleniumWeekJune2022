package sel.week10.day1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.framework.BaseClass;

public class AmazonTest extends BaseClass{
	
	
	
	@Test
	public void titleTest() {
		
		String title = BaseClass.driver.getTitle();
		Assert.assertEquals(title, "Online Shopping site in India: Shop Online for Mobiles, Books, Watches, Shoes and More - Amazon.in");
		
		
	}
	
	@Test(dependsOnMethods="titleTest")
	public void logoTest() {
		
		WebElement logo = BaseClass.driver.findElement(By.id("nav-logo-sprites"));
		Assert.assertTrue(logo.isDisplayed());
		
	}
	
	

}
