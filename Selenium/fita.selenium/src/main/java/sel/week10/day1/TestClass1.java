package sel.week10.day1;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.BaseClass;

public class TestClass1 extends BaseClass {

	@BeforeTest
	public void testDataSet() {

		System.out.println("BT - create user 1");

	}

	@Test
	public void test1() {

		System.out.println("test 1");

	}

	@Test
	public void test2() {

		System.out.println("test 2");

	}

	@Test
	public void test3() {

		System.out.println("test 3");

	}

}
